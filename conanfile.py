
import os

from conans import ConanFile, CMake, tools

project_name = "boost_text"


class BoostTextConan(ConanFile):
    name = project_name
    version = "1.0.0"
    url = "https://github.com/tzlaine/text"
    description = "boost_text recipe used in asd project"
    topics = ()
    generators = "cmake"
    settings = "os", "compiler", "build_type", "arch"
    exports_sources = "CMakeLists.txt", "asd.json"
    requires = (
        "asd.build_tools/0.0.1@asd/testing",
        "boost/[>=1.66]",
    )

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.hpp", dst="include", src="boost_text/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        if not self.in_local_cache:
            self.cpp_info.includedirs = ["boost_text/include"]
            self.cpp_info.libdirs = ["boost_text/lib"]
        self.cpp_info.libs = ['boost_text']
